#include <algorithm>
#include <cfloat>
#include <vector>

#include "caffe/layer.hpp"
#include "caffe/util/math_functions.hpp"
#include "caffe/vision_layers.hpp"

namespace caffe {

template <typename Dtype>
__global__ void SoftmaxLossForwardGPU(const int nthreads,
          const Dtype* prob_data, const Dtype* label, Dtype* loss,
          const int num, const int dim, const int spatial_dim,
          const bool has_ignore_label_, const int ignore_label_,
          Dtype* counts,  Dtype* fc_device, Dtype fc_inv_norm) {


  CUDA_KERNEL_LOOP(index, nthreads) {
    const int n = index / spatial_dim;
    const int s = index % spatial_dim;
    const int label_value = static_cast<int>(label[n * spatial_dim + s]);
    if (has_ignore_label_ && label_value == ignore_label_) {
      loss[index] = 0;
      counts[index] = 0;
    } else {
      loss[index] = -log(max(prob_data[n * dim + label_value * spatial_dim + s],
                      Dtype(FLT_MIN)))/ (fc_device[label_value] * fc_inv_norm);
      counts[index] = 1;
    }
  }
}


template <typename Dtype>
void SoftmaxWithLossLayer<Dtype>::Forward_gpu(
    const vector<Blob<Dtype>*>& bottom, const vector<Blob<Dtype>*>& top) {
  softmax_layer_->Forward(softmax_bottom_vec_, softmax_top_vec_);
  const Dtype* prob_data = prob_.gpu_data();
  const Dtype* label = bottom[1]->gpu_data();
  const int dim = prob_.count() / outer_num_;
  const int nthreads = outer_num_ * inner_num_;
  // Since this memory is not used for anything until it is overwritten
  // on the backward pass, we use it here to avoid having to allocate new GPU
  // memory to accumulate intermediate results in the kernel.
  Dtype* loss_data = bottom[0]->mutable_gpu_diff();
  // Similarly, this memory is never used elsewhere, and thus we can use it
  // to avoid having to allocate additional GPU memory.
  Dtype* counts = prob_.mutable_gpu_diff();
  // NOLINT_NEXT_LINE(whitespace/operators)
  int count = 0;

  if (fc != NULL)
      free(fc);

  //Compute Max label value to know how many fc we need
  int maxLabelValue = -1;
  const Dtype* labels = bottom[1]->cpu_data();

  for (int i = 0; i < outer_num_; ++i) {
      for (int j = 0; j < inner_num_; j++) {

          const int label_value = static_cast<int>(labels[i * inner_num_ + j]);
          if (has_ignore_label_ && label_value == ignore_label_) {
              continue;
          }
          DCHECK_GE(label_value, 0);
          DCHECK_LT(label_value, prob_.shape(softmax_axis_));
          if (maxLabelValue < label_value)
              maxLabelValue = label_value;
      }
  }

  //Count number of occurrencies per class and normalize by total count
  maxLabelValue += 1;
  fc = (Dtype*)malloc(sizeof(Dtype) * maxLabelValue);
  memset(fc, 0, sizeof(Dtype) * maxLabelValue);
  count = 0;
  for (int i = 0; i < outer_num_; ++i) {
      for (int j = 0; j < inner_num_; j++) {
          const int label_value = static_cast<int>(labels[i * inner_num_ + j]);
          if (has_ignore_label_ && label_value == ignore_label_) {
              continue;
          }
          DCHECK_GE(label_value, 0);
          DCHECK_LT(label_value, prob_.shape(softmax_axis_));
          fc[label_value] += Dtype(1);
          ++count;
      }
  }

  for (int i=0; i<maxLabelValue; i++)
      fc[i] /= count;

  fc_inv_norm = 0;
  for (int i=0; i<maxLabelValue; i++)
      if (fc[i] != Dtype(0))
          fc_inv_norm += Dtype(1) / fc[i];



  // Memory allocation
  cudaMalloc((void**) &fc_device, sizeof(Dtype)*maxLabelValue);

  // Transfer
  cudaMemcpy(fc_device, fc, sizeof(Dtype)*maxLabelValue, cudaMemcpyHostToDevice);

  //Compute loss
  SoftmaxLossForwardGPU<Dtype><<<CAFFE_GET_BLOCKS(nthreads),
      CAFFE_CUDA_NUM_THREADS>>>(nthreads, prob_data, label, loss_data,
      outer_num_, dim, inner_num_, has_ignore_label_, ignore_label_, counts, fc_device,
      fc_inv_norm);



  //Normalize by number of batch
  Dtype loss;
  caffe_gpu_asum(nthreads, loss_data, &loss);

  if (normalize_) {
    Dtype count;
    caffe_gpu_asum(nthreads, counts, &count);
    loss /= count;
  } else {
    loss /= outer_num_;
  }

  top[0]->mutable_cpu_data()[0] = loss;
  if (top.size() == 2) {
    top[1]->ShareData(prob_);
  }
}

template <typename Dtype>
__global__ void SoftmaxLossBackwardGPU(const int nthreads, const Dtype* top,
          const Dtype* label, Dtype* bottom_diff, const int num, const int dim,
          const int spatial_dim, const bool has_ignore_label_,
          const int ignore_label_, Dtype* counts, Dtype* fc_device, Dtype fc_inv_norm )
{
  const int channels = dim / spatial_dim;

  CUDA_KERNEL_LOOP(index, nthreads) {
    const int n = index / spatial_dim;
    const int s = index % spatial_dim;
    const int label_value = static_cast<int>(label[n * spatial_dim + s]);

    if (has_ignore_label_ && label_value == ignore_label_) {
      for (int c = 0; c < channels; ++c) {
        bottom_diff[n * dim + c * spatial_dim + s] = 0;
      }
      counts[index] = 0;
    } else {
      bottom_diff[n * dim + label_value * spatial_dim + s] -= 1;
      const Dtype beta = Dtype(1) / (fc_device[label_value] * fc_inv_norm);
      for (int k=0; k<channels; k++)
          bottom_diff[n * dim + k * spatial_dim + s] *= beta;
      counts[index] = 1;
    }
  }


}

template <typename Dtype>
void SoftmaxWithLossLayer<Dtype>::Backward_gpu(const vector<Blob<Dtype>*>& top,
    const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
  if (propagate_down[1]) {
    LOG(FATAL) << this->type()
               << " Layer cannot backpropagate to label inputs.";
  }
  if (propagate_down[0]) {
    Dtype* bottom_diff = bottom[0]->mutable_gpu_diff();
    const Dtype* prob_data = prob_.gpu_data();
    const Dtype* top_data = top[0]->gpu_data();
    caffe_gpu_memcpy(prob_.count() * sizeof(Dtype), prob_data, bottom_diff);
    const Dtype* label = bottom[1]->gpu_data();
    const int dim = prob_.count() / outer_num_;
    const int nthreads = outer_num_ * inner_num_;
    // Since this memory is never used for anything else,
    // we use to to avoid allocating new GPU memory.
    Dtype* counts = prob_.mutable_gpu_diff();
    // NOLINT_NEXT_LINE(whitespace/operators)


    //Compute gradient
    SoftmaxLossBackwardGPU<Dtype><<<CAFFE_GET_BLOCKS(nthreads),
        CAFFE_CUDA_NUM_THREADS>>>(nthreads, top_data, label, bottom_diff,
        outer_num_, dim, inner_num_, has_ignore_label_, ignore_label_, counts, fc_device,
        fc_inv_norm);

    //Scale gradient
    const Dtype loss_weight = top[0]->cpu_diff()[0];
    if (normalize_) {
      Dtype count;
      caffe_gpu_asum(nthreads, counts, &count);
      caffe_gpu_scal(prob_.count(), loss_weight / count, bottom_diff);
    } else {
      caffe_gpu_scal(prob_.count(), loss_weight / outer_num_, bottom_diff);
    }
    cudaMemcpy( fc, fc_device, sizeof(Dtype)*maxLabelValue, cudaMemcpyDeviceToHost);

    cudaFree(fc_device);

  }
}

INSTANTIATE_LAYER_GPU_FUNCS(SoftmaxWithLossLayer);

}  // namespace caffe
